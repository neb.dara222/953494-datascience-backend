import pytest
import requests

url = 'http://127.0.0.1:5000'  # The root url of the flask app

#test prediction_bayesian_model endpoint
#size
def test_prediction_bayesian_model_size_fail():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']) != 0

def test_prediction_bayesian_model_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']) == 1

#model key size
def test_prediction_bayesian_model_model_key_size_fail():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']) != 0

def test_prediction_bayesian_model_model_key_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']) == 4

#model --> adjusted_dates key
def test_prediction_bayesian_model_model_key_adjusted_dates_key_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']['adjusted_dates']) != 0

def test_prediction_bayesian_model_model_key_adjusted_dates_key_not_empty():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert data['prediction_models']['model']['adjusted_dates'] != ''

#model --> future_forcast key
def test_prediction_bayesian_model_model_key_future_forcast_key_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']['future_forcast']) != 0

def test_prediction_bayesian_model_model_key_future_forcast_key_not_empty():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert data['prediction_models']['model']['future_forcast'] != ''

#model --> prediction key
def test_prediction_bayesian_model_model_key_prediction_key_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']['prediction']) != 0

def test_prediction_bayesian_model_model_key_prediction_key_not_empty():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert data['prediction_models']['model']['prediction'] != ''

#model --> world_cases key
def test_prediction_bayesian_model_model_key_world_cases_key_size_success():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert len(data['prediction_models']['model']['world_cases']) != 0

def test_prediction_bayesian_model_model_key_world_cases_key_not_empty():
    r = requests.get(url+'/prediction_bayesian_model')
    data = r.json()
    assert r.status_code == 200
    assert data['prediction_models']['model']['world_cases'] != ''
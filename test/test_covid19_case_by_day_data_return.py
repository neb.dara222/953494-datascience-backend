import pytest
import requests

url = 'http://127.0.0.1:5000'  # The root url of the flask app

def test_hello_data_return_success():
    r = requests.get(url+'/')
    data = r.json()
    assert r.status_code == 200
    assert data['message'] == 'Hello, World!'

def test_hello_data_return_fail():
    r = requests.get(url+'/')
    data = r.json()
    assert r.status_code == 200
    assert data['message'] != ''

#test covid19_case_by_day endpoint
#size
def test_covid19_case_by_day_size_fail():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']) != 0

def test_covid19_case_by_day_size_success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']) == 7

#Confimed
def test_covid19_case_by_day_confimed_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['Confirmed']) != 0

def test_covid19_case_by_day_confimed_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['Confirmed'] != ''

#Date
def test_covid19_case_by_day_date_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['Date']) != 0

def test_covid19_case_by_day_date_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['Date'] != ''

#Deaths
def test_covid19_case_by_day_deaths_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['Deaths']) != 0

def test_covid19_case_by_day_deaths_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['Deaths'] != ''

#Recovered
def test_covid19_case_by_day_recovered_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['Recovered']) != 0

def test_covid19_case_by_day_recovered_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['Recovered'] != ''

#newConfirmed
def test_covid19_case_by_day_newConfirmed_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['newConfirmed']) != 0

def test_covid19_case_by_day_newConfirmed_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['newConfirmed'] != ''

#newDeath
def test_covid19_case_by_day_newDeath_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['newDeath']) != 0

def test_covid19_case_by_day_newDeath_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['newDeath'] != ''

#newRecovered
def test_covid19_case_by_day_newRecovered_key_size__success():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_case']['newRecovered']) != 0

def test_covid19_case_by_day_newRecovered_key_not_empty():
    r = requests.get(url+'/covid19_case_by_day')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_case']['newRecovered'] != ''
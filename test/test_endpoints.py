import pytest
import requests

url = 'http://127.0.0.1:5000'  # The root url of the flask app


def test_hello_success():
    r = requests.get(url+'/')
    assert r.status_code == 200


def test_hello_fail():
    r = requests.get(url+'/hello')
    assert r.status_code != 200


def test_covid19_case_by_day_success():
    r = requests.get(url+'/covid19_case_by_day')
    assert r.status_code == 200

def test_covid19_case_by_day_fail():
    r = requests.get(url+'/covid19_case_by_day/')
    assert r.status_code != 200


def test_prediction_svm_model_success():
    r = requests.get(url+'/prediction_svm_model')
    assert r.status_code == 200


def test_prediction_svm_model_fail():
    r = requests.get(url+'/prediction_svm_model/')
    assert r.status_code != 200


def test_prediction_bayesian_model_success():
    r = requests.get(url+'/prediction_bayesian_model')
    assert r.status_code == 200


def test_prediction_bayesian_model_fail():
    r = requests.get(url+'/prediction_bayesian_model/')
    assert r.status_code != 200


def test_prediction_linear_regression_model_success():
    r = requests.get(url+'/prediction_linear_regression_model')
    assert r.status_code == 200


def test_prediction_linear_regression_model_fail():
    r = requests.get(url+'/prediction_linear_regression_model/')
    assert r.status_code != 200

def test_covid19_case_by_age_success():
    r = requests.get(url+'/covid19_case_by_age')
    assert r.status_code == 200

def test_covid19_case_by_age_fail():
    r = requests.get(url+'/covid19_case_by_age/')
    assert r.status_code != 200
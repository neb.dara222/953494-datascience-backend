import pytest
import requests

url = 'http://127.0.0.1:5000'  # The root url of the flask app

#test covid19_case_by_age endpoint
#size
def test_covid19_case_by_age_size_fail():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']) != 0

def test_covid19_case_by_age_size_success():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']) == 4

#age
def test_covid19_case_by_age_age_key_size_success():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']['age']) != 0

def test_covid19_case_by_age_age_key_not_empty():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_agecase']['age'] != ''

#confirmed
def test_covid19_case_by_age_confirmed_key_size_success():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']['confirmed']) != 0

def test_covid19_case_by_age_confirmed_key_not_empty():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_agecase']['confirmed'] != ''

#death
def test_covid19_case_by_age_death_key_size_success():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']['death']) != 0

def test_covid19_case_by_age_death_key_not_empty():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_agecase']['death'] != ''

#recovered
def test_covid19_case_by_age_recovered_key_size_success():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert len(data['accumulate_agecase']['recovered']) != 0

def test_covid19_case_by_age_recovered_key_not_empty():
    r = requests.get(url+'/covid19_case_by_age')
    data = r.json()
    assert r.status_code == 200
    assert data['accumulate_agecase']['recovered'] != ''
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 01:09:34 2020

@author: berry
"""


import numpy as np
import pandas as pd
import re
from flask_restful import Resource
from csv import reader


df_cases = pd.read_csv('./world_data/COVID19_line_list_data.csv')

asd=list(['male','female'])
df_cases=df_cases[df_cases['gender'].isin(asd)]
df_cases['confirmed']='1'
df_cases = df_cases[['gender','death','confirmed','recovered']]
df_cases.to_csv("./gender.csv",index=False,sep=',')



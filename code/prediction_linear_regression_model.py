import numpy as np
import pandas as pd
import datetime
from sklearn.linear_model import LinearRegression, BayesianRidge
from sklearn.model_selection import RandomizedSearchCV, train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from flask_restful import Resource
from csv import reader
from flask.json import jsonify


class PredictionLinearRegressionModel(Resource):
    def get(self):
        df_cases = pd.read_csv(
            './assets/world_data/COVID_Data_Basic_UPDATED_LATEST.csv')

        df_cases = pd.DataFrame.from_dict(df_cases)
        df_cases = df_cases.groupby('Date', as_index=False)[
            'Confirmed', 'Death', 'Recovered'].sum()
        cases_dict = {}
        dates = df_cases['Date'].to_numpy()
        world_cases = df_cases['Confirmed'].to_numpy()

        days_since_1_22 = np.array(
            [i for i in range(len(dates))]).reshape(-1, 1)
        days_in_future = 7
        future_forcast = np.array(
            [i for i in range(len(dates)+days_in_future)]).reshape(-1, 1)
        world_cases = np.array(world_cases).reshape(-1, 1)

        adjusted_dates = future_forcast[:-10]
        start = '12/31/2019'
        start_date = datetime.datetime.strptime(start, '%m/%d/%Y')
        future_forcast_dates = []
        for i in range(len(future_forcast)):
            future_forcast_dates.append(
                (start_date + datetime.timedelta(days=i)).strftime('%m/%d/%Y'))
        X_train_confirmed, X_test_confirmed, y_train_confirmed, y_test_confirmed = train_test_split(
            days_since_1_22, world_cases, test_size=0.15, shuffle=False)

        svm_confirmed = SVR(shrinking=True, kernel='poly',
                            gamma=0.01, epsilon=1, degree=6, C=0.1)
        svm_confirmed.fit(X_train_confirmed, y_train_confirmed)
        svm_pred = svm_confirmed.predict(future_forcast)
        # check against testing data
        # svm prediction
        svm_test_pred = svm_confirmed.predict(X_test_confirmed)
        print('MAE:', mean_absolute_error(svm_test_pred, y_test_confirmed))
        print('MSE:', mean_squared_error(svm_test_pred, y_test_confirmed))
        print('Coefficient of determination: %.2f'
              % r2_score(y_test_confirmed, svm_test_pred))
        world_cases = world_cases[:-3]

        # transform our data for polynomial regression
        poly = PolynomialFeatures(degree=6)
        poly_X_train_confirmed = poly.fit_transform(X_train_confirmed)
        poly_X_test_confirmed = poly.fit_transform(X_test_confirmed)
        poly_future_forcast = poly.fit_transform(future_forcast)

        # polynomial regression
        linear_model = LinearRegression(normalize=True, fit_intercept=False)
        linear_model.fit(poly_X_train_confirmed, y_train_confirmed)
        test_linear_pred = linear_model.predict(poly_X_test_confirmed)
        linear_pred = linear_model.predict(poly_future_forcast)

        prediction_confirmed_world_case = {'model': {'world_cases': world_cases.tolist(),
                                                     'adjusted_dates': adjusted_dates.tolist(), 'future_forcast': future_forcast.tolist(), 'prediction': linear_pred.tolist()}}
        return jsonify({'prediction_models': prediction_confirmed_world_case})

import numpy as np
import pandas as pd
from flask_restful import Resource
from csv import reader
from flask.json import jsonify


class Covid19CaseByDay(Resource):
    def get(self):
        df_cases = pd.read_csv(
            './assets/world_data/COVID_Data_Basic_UPDATED_LATEST.csv')
        df_cases = pd.DataFrame.from_dict(df_cases)
        df_cases = df_cases.groupby('Date', as_index=False)[
            'Confirmed', 'Death', 'Recovered', 'newConfirmed', 'newDeath', 'newRecovered'].sum()
        cases_dict = {}
        cases_dict['Date'] = df_cases['Date'].to_numpy().tolist()
        cases_dict['Confirmed'] = df_cases['Confirmed'].to_numpy().tolist()
        cases_dict['Deaths'] = df_cases['Death'].to_numpy().tolist()
        cases_dict['Recovered'] = df_cases['Recovered'].to_numpy().tolist()
        cases_dict['newConfirmed'] = df_cases['newConfirmed'].to_numpy().tolist()
        cases_dict['newDeath'] = df_cases['newDeath'].to_numpy().tolist()
        cases_dict['newRecovered'] = df_cases['newRecovered'].to_numpy().tolist()

        return jsonify({'accumulate_case': cases_dict})

import numpy as np
import pandas as pd
from flask_restful import Resource
from csv import reader
from flask.json import jsonify


class Covid19CaseByAge(Resource):
    def get(self):
        df_cases = pd.read_csv('./assets/age.csv')
        df_cases = pd.DataFrame.from_dict(df_cases)
        df_cases = df_cases.groupby('age', as_index=False)['confirmed', 'death','recovered'].sum()
        cases_dict = {}
        cases_dict['age'] = df_cases['age'].to_numpy().tolist()
        cases_dict['confirmed'] = df_cases['confirmed'].to_numpy().tolist()
        cases_dict['death'] = df_cases['death'].to_numpy().tolist()
        cases_dict['recovered'] = df_cases['recovered'].to_numpy().tolist()

        return jsonify({'accumulate_agecase': cases_dict})

import numpy as np
import pandas as pd
import datetime
from sklearn.linear_model import LinearRegression, BayesianRidge
from sklearn.model_selection import RandomizedSearchCV, train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from flask_restful import Resource
from csv import reader
from flask.json import jsonify


class PredictionBayesianModel(Resource):
    def get(self):
        df_cases = pd.read_csv(
            './assets/world_data/COVID_Data_Basic_UPDATED_LATEST.csv')
        df_cases = pd.DataFrame.from_dict(df_cases)
        df_cases = df_cases.groupby('Date', as_index=False)[
            'Confirmed', 'Death', 'Recovered', 'newConfirmed', 'newDeath', 'newRecovered'].sum()
        cases_dict = {}
        dates = df_cases['Date'].to_numpy()
        world_cases = df_cases['Confirmed'].to_numpy()

        days_since_1_22 = np.array(
            [i for i in range(len(dates))]).reshape(-1, 1)
        days_in_future = 7
        future_forcast = np.array(
            [i for i in range(len(dates)+days_in_future)]).reshape(-1, 1)
        world_cases = np.array(world_cases).reshape(-1, 1)

        adjusted_dates = future_forcast[:-10]
        start = '12/31/2019'
        start_date = datetime.datetime.strptime(start, '%m/%d/%Y')
        future_forcast_dates = []
        for i in range(len(future_forcast)):
            future_forcast_dates.append(
                (start_date + datetime.timedelta(days=i)).strftime('%m/%d/%Y'))
        X_train_confirmed, X_test_confirmed, y_train_confirmed, y_test_confirmed = train_test_split(
            days_since_1_22, world_cases, test_size=0.15, shuffle=False)

        world_cases = world_cases[:-3]

        # transform our data for polynomial regression
        poly = PolynomialFeatures(degree=6)
        poly_X_train_confirmed = poly.fit_transform(X_train_confirmed)
        poly_X_test_confirmed = poly.fit_transform(X_test_confirmed)
        poly_future_forcast = poly.fit_transform(future_forcast)

        # polynomial regression
        linear_model = LinearRegression(normalize=True, fit_intercept=False)
        linear_model.fit(poly_X_train_confirmed, y_train_confirmed)
        test_linear_pred = linear_model.predict(poly_X_test_confirmed)
        linear_pred = linear_model.predict(poly_future_forcast)

        # bayesian ridge polynomial regression
        tol = [1e-4, 1e-3, 1e-2]
        alpha_1 = [1e-7, 1e-6, 1e-5, 1e-4]
        alpha_2 = [1e-7, 1e-6, 1e-5, 1e-4]
        lambda_1 = [1e-7, 1e-6, 1e-5, 1e-4]
        lambda_2 = [1e-7, 1e-6, 1e-5, 1e-4]

        bayesian_grid = {'tol': tol, 'alpha_1': alpha_1,
                         'alpha_2': alpha_2, 'lambda_1': lambda_1, 'lambda_2': lambda_2}

        bayesian = BayesianRidge(fit_intercept=False, normalize=True)
        bayesian_search = RandomizedSearchCV(bayesian, bayesian_grid, scoring='neg_mean_squared_error',
                                             cv=3, return_train_score=True, n_jobs=-1, n_iter=40, verbose=1)
        bayesian_search.fit(poly_X_train_confirmed, y_train_confirmed.ravel())
        bayesian_search.best_params_
        bayesian_confirmed = bayesian_search.best_estimator_
        test_bayesian_pred = bayesian_confirmed.predict(poly_X_test_confirmed)
        bayesian_pred = bayesian_confirmed.predict(poly_future_forcast)
        print('MAE:', mean_absolute_error(test_bayesian_pred, y_test_confirmed))
        print('MSE:', mean_squared_error(test_bayesian_pred, y_test_confirmed))

        prediction_confirmed_world_case = {'model':  {'world_cases': world_cases.tolist(),
                                                      'adjusted_dates': adjusted_dates.tolist(), 'future_forcast': future_forcast.tolist(), 'prediction': bayesian_pred.tolist()}}
        return jsonify({'prediction_models': prediction_confirmed_world_case})

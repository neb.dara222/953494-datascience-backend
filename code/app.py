import numpy as np
import pandas as pd
import random
import math
import time
import datetime
from flask import Flask
from flask_restful import Api
from flask_cors import CORS
from covid19_case_by_age import Covid19CaseByAge
from covid19_case_by_day import Covid19CaseByDay
from covid19_case_by_gender import Covid19CaseByGender
from prediction_svm_model import PredictionSVMModel
from prediction_bayesian_model import PredictionBayesianModel
from prediction_linear_regression_model import PredictionLinearRegressionModel
from csv import reader

app = Flask(__name__)
CORS(app)
app.secret_key = 'dara'
api = Api(app)

@app.route('/', methods=['GET'])
def hello():
    return {'message': 'Hello, World!'}


api.add_resource(Covid19CaseByAge, '/covid19_case_by_age')
api.add_resource(Covid19CaseByDay, '/covid19_case_by_day')
api.add_resource(Covid19CaseByGender, '/covid19_case_by_gender')
api.add_resource(PredictionSVMModel, '/prediction_svm_model')
api.add_resource(PredictionBayesianModel, '/prediction_bayesian_model')
api.add_resource(PredictionLinearRegressionModel,
                 '/prediction_linear_regression_model')

if __name__ == '__main__':
    app.run(port=5000, debug=True)

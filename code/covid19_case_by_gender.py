#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  9 01:31:39 2020

@author: berry
"""

import numpy as np
import pandas as pd
from flask_restful import Resource
from csv import reader
from flask.json import jsonify


class Covid19CaseByGender(Resource):
    def get(self):
        df_cases = pd.read_csv('./assets/gender.csv')
        df_cases = pd.DataFrame.from_dict(df_cases)
        df_cases = df_cases.groupby('gender', as_index=False)['confirmed', 'death','recovered'].sum()
        cases_dict = {}
        cases_dict['gender'] = df_cases['gender'].to_numpy().tolist()
        cases_dict['confirmed'] = df_cases['confirmed'].to_numpy().tolist()
        cases_dict['death'] = df_cases['death'].to_numpy().tolist()
        cases_dict['recovered'] = df_cases['recovered'].to_numpy().tolist()

        return jsonify({'accumulate_gendercase': cases_dict})

